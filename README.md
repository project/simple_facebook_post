## Simple Facebook Post

Post content on your facebook page 

This module allows you to post on a Facebook page from the content created on
your website.
### Features

* create a post on a facebook page automatically.

### Install

Install module via composer.

### Usage

* Install the hook post action module.
* Install the Publishing Options module and Create a publishing option for the
type of content you want to use.
* Configure the type of content you want to use, enter the system name of the
content, and the system name of the field from which the text will be obtained.
* Select the Publishing Options created above for the content type.
* Configure the Facebook Graph api access. You should create a Facebook APP.
* Soon, there will be a step-by-step guide on how to use this module.
