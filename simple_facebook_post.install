<?php

/**
 * @file
 * The modulename.install file.
 */

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Implements hook_schema().
 */
function simple_facebook_post_schema() {

  // Image Field Caption table.
  $schema['sfp_post'] = [
    'description' => 'Link a Facebook page post with a node.',
    'fields' => [
      'id' => [
        'description' => 'The primary key of row',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'entity_id' => [
        'description' => 'The entity id attached to this post',
        'type' => 'int',
        'length' => 11,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'post_content' => [
        'description' => 'The content of post',
        'type'        => 'varchar',
        'length'      => 512,
        'not null'    => TRUE,
        'default'     => '',
      ],
      'post_id' => [
        'description' => 'Post id Provided by facebook',
        'type'        => 'varchar',
        'length'      => 128,
        'not null'    => TRUE,
        'default'     => '',
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp created.',
      ],
    ],
    'unique keys' => [
      'id' => ['id'],
    ],
    'indexes' => [
      'entity_id' => ['entity_id'],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}

/**
 * Implements hook_install().
 */
function simple_facebook_post_install() {

  $config = \Drupal::service('config.factory')
    ->getEditable('simple_facebook_post.settings');

  $up_default = $config->get('up_timestamp');
  if ($up_default == 0) {
    // In a special case you could enter a non-zero time stamp.
    $up_timestamp = new DrupalDateTime('now');
    // Set and save new message value.
    $config->set('up_timestamp', $up_timestamp->getTimestamp())->save();
  }
}
