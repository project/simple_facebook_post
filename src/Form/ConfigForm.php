<?php

namespace Drupal\simple_facebook_post\Form;

use Facebook\Facebook as Facebook;
use Drupal\Core\Form\ConfigFormBase;
use Facebook\Exception\SDKException;
use Drupal\Core\Form\FormStateInterface;
use Facebook\Exception\ResponseException;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class for config form.
 */
class ConfigForm extends ConfigFormBase {
  /**
   * For use the Drupal state api.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructor method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The interface for Config Factory.
   * @param \Drupal\Core\State\State $state
   *   The object State.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    State $state,
    MessengerInterface $messenger
    ) {
    parent::__construct($config_factory);

    $this->state = $state;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('messenger')
    );
  }

  /**
   * Get Form Id Method.
   */
  public function getFormId() {
    return 'simple_facebook_post';

  }

  /**
   * Get Editable Config Names Method.
   */
  public function getEditableConfigNames() {
    return [
      'simple_facebook_post.settings',
    ];
  }

  /**
   * Build Form method.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $module_name = 'simple_facebook_post';
    $config      = $this->config($module_name . '.settings');
    $app_id      = $this->state->get($module_name . '.app_id', '');
    $app_secret  = $this->state->get($module_name . '.app_secret', '');
    $token       = $this->state->get($module_name . '.page_access_token', '');
    $page_id     = $this->state->get($module_name . '.page_id', '');
    $form        = parent::buildForm($form, $form_state);
    $facebook    = \Drupal::service('simple_facebook_post.fb_wapper');

    $form['sections'] = [
      '#type'         => 'vertical_tabs',
      '#title'        => $this->t('Settings'),
      '#default_tab'  => 'edit-content-box',
    ];
    $form['content_box'] = [
      '#type'         => 'details',
      '#title'        => $this->t('Content Settings'),
      '#group'        => 'sections',
      '#description'  => $this->t('
          Select the type of content and the 
          fields that will be used for sharing on 
          social networks when the content is created.'
      ),
    ];
    $form['content_box']['content'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Content bundle'),
      '#default_value'  => $config->get('content'),
      '#description'    => $this->t('
          Select the type of content you want to use to be published 
          on social networks at the time of its creation.
        '),
    ];

    $form['content_box']['body'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t(
        'Field to use to generate the text of the post'
      ),
      '#default_value'  => $config->get('body'),
      '#description'    => $this->t(
        'Select the field you want to use as a font for the body of the post.
        If the text is too long, it will be cut off.'
      ),
    ];

    $form['content_box']['body_use_summary'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t(
        'Use summary if available for selected field in post body.'
      ),
      '#default_value'  => $config->get('body_use_summary'),
    ];
    $form['content_box']['share_link'] = [
      '#type'           => 'checkbox',
      '#title'          => $this->t('Attach the link to the content'),
      '#default_value'  => $config->get('share_link'),
    ];
    $form['content_box']['custom_link_op'] = [
      '#type'         => 'fieldset',
      '#title'        => $this->t('Link override options'),
      '#states'       => [
        'visible'   => [':input[name="share_link"]' => ['checked' => TRUE]],
        'invisible' => [':input[name="share_link"]' => ['checked' => FALSE]],
      ],
      '#descriptions' => $this->t(
        'link override options'
      ),
    ];
    $form['content_box']['custom_link_op']['mssg'] = [
      '#type' => 'markup',
      '#markup' => '<p class="alert-mg">Attention! to use these options your
       domain must be validated in the facebook app you are using.</p>',
    ];

    $form['content_box']['custom_link_op']['link_title'] = [
      '#type'           => 'textfield',
      '#disabled'       => TRUE,
      '#title'          => $this->t(
        'Field to use to generate the title of the post'
      ),
      '#default_value'  => $config->get('link_title'),
      '#description'    => $this->t(
        'Select the field you want to use as the title of the link.
          Link title customization options are available only if you have 
          validated your website domain on Facebook.'
      ),
    ];

    $form['content_box']['custom_link_op']['link_title_suffix'] = [
      '#type'           => 'textfield',
      '#disabled'       => TRUE,
      '#title'          => $this->t('Title suffx'),
      '#default_value'  => $config->get('link_title_suffix'),
      '#description'    => $this->t(
        'Enter a fixed text or a token to concatenate to the end of the title of
         link. Link title customization options are available only if you have 
         validated your website domain on Facebook.'
      ),
    ];
    $form['content_box']['custom_link_op']['link_image'] = [
      '#type'           => 'textfield',
      '#disabled'       => TRUE,
      '#title'          => $this->t(
        'Field to use to generate the image of the post'
      ),
      '#description'    => $this->t(
        'Select the field you want to use for the post image.'
      ),
      '#default_value'  => $config->get('link_image'),
    ];

    $form['content_box']['custom_link_op']['link_image_style'] = [
      '#type'           => 'textfield',
      '#disabled'       => TRUE,
      '#title'          => $this->t('Post Image Style'),
      '#default_value'  => $config->get('link_image_style'),
      '#empty_option'   => $this->t('Select'),
      '#description'    => $this->t(
        'Enter the image style you want to use, 
          if you do not select any option the original image will be used. Some
          available options are @styles',
          [
            '@styles' => $this->getImageStyles(),
          ]
      ),
    ];

    // Servicio provisto por pub_options.
    $publishing_options = \Drupal::service('publishing_options.content');
    $po                 = $publishing_options->getPublishingOptions();
    $options            = [];

    foreach ($po as $item) {
      $options[$item->pubid] = $item->title;
    }
    $form['content_box']['pubid'] = [
      '#type'           => 'select',
      '#title'          => $this->t('Publishing option'),
      '#options'        => $options,
      '#description'    => $this->t(
        'Publication option that indicates if the content should be published on
         Facebook page or not.'
      ),
      '#default_value'  => $config->get('pub_option_id'),
    ];
    $form['facebook'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Facebook API Access Settings'),
      '#group'  => 'sections',
    ];
    $status = $facebook::getFacebookConnectionStatus();
    $form['facebook']['status'] = [
      '#type'   => 'markup',
      '#markup' => $status['message'] ?? '',
    ];
    $form['facebook']['app_id'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Facebook app id'),
      '#description'    => $this->t('Facebook app id'),
      '#default_value'  => $app_id,
    ];
    $form['facebook']['app_secret'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Facebook app secret'),
      '#description'    => $this->t('Facebook app secret'),
      '#default_value'  => $app_secret,
    ];
    $form['facebook']['page_id'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Facebook page id'),
      '#description'    => $this->t('Facebook page id'),
      '#default_value'  => $page_id,
    ];
    $form['facebook']['page_access_token'] = [
      '#type'           => 'textarea',
      '#title'          => $this->t('Facebook access token'),
      '#description'    => $this->t('Facebook access token'),
      '#default_value'  => $token,
    ];

    $form['facebook']['api_version'] = [
      '#type'           => 'textfield',
      '#title'          => $this->t('Facebook Graph api version'),
      '#description'    => $this->t(
        'Enter the version of the API to use to interact with facebook. By
         default v13.0.'
      ),
      '#default_value'  => $config->get('api_version'),
    ];

    return $form;
  }

  /**
   * Form submit.
   *
   * { @inheritDoc }
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $module_name = 'simple_facebook_post';
    $config = $this->config($module_name . '.settings');
    $config->set(
      'content',
      $form_state->getValue('content')
    );
    $config->set(
      'body',
      $form_state->getValue('body')
    );
    $config->set(
      'body_use_summary',
      $form_state->getValue('body_use_summary')
    );
    $config->set(
      'share_link',
      $form_state->getValue('share_link')
    );
    $config->set(
      'link_title',
      $form_state->getValue('link_title')
    );
    $config->set(
      'link_title_suffix',
      $form_state->getValue('link_title_suffix')
    );
    $config->set(
      'link_image',
      $form_state->getValue('link_image')
    );
    $config->set(
      'link_image_style',
      $form_state->getValue('link_image_style')
    );
    $config->set(
      'pub_option_id',
      $form_state->getValue('pubid')
    );
    $config->set(
      'api_version',
      $form_state->getValue('api_version')
    );
    $config->save();

    $this->state->set(
      $module_name . '.app_id',
      $form_state->getValue('app_id')
    );
    $this->state->set(
      $module_name . '.app_secret',
      $form_state->getValue('app_secret')
    );
    $this->state->set(
      $module_name . '.page_id',
      $form_state->getValue('page_id')
    );
    $this->state->set(
      $module_name . '.page_access_token',
      $form_state->getValue('page_access_token')
    );

    $app_id      = $form_state->getValue('app_id');
    $app_secret  = $form_state->getValue('app_secret');
    $token       = $form_state->getValue('page_access_token');
    $api_version = $form_state->getValue('api_version');

    if (empty($app_id) || empty($app_secret) || empty($token)) {
      // Nada que probar.
      return parent::submitForm($form, $form_state);
    }

    try {
      $fb = new Facebook([
        'app_id'                => $app_id,
        'app_secret'            => $app_secret,
        'default_graph_version' => $api_version ?? 'v13.0',
        'default_access_token'  => $token,
      ]);

      $response_user  = $fb->get('/me?fields=name');
      $user_name      = $response_user->getGraphUser()->getName();
      $token          = $form_state->getValue('page_access_token');
      $debug_token_ep = '/debug_token?input_token=' . $token
       . '&access_token=' . $token;

      // Token info.
      $debug_token = $fb->get($debug_token_ep);
      $decodedBody = $debug_token->getDecodedBody();
      // 0 or Unixtime
      $expires_at   = $decodedBody['data']["expires_at"];
      $managedPages = $fb->get('me/accounts?type=page');
      $pages        = $managedPages->getDecodedBody();
      $page_id      = $form_state->getValue('page_id');
      $page_msg     = $this->t(
        'The entered page does not exist or is not managed by the user.'
      );

      foreach ($pages['data'] as $p) {
        if ($p['id'] == $page_id) {
          $page_msg = $this->t(
            'The @pname page was entered and the user is your administrator.',
            ['@pname' => $p['name']]
          );
          break;
        }
      }
      if ($expires_at !== 0) {
        // No es un token de larga duracion
        // Pedimos a graph un nuevo token de larga duracion.
        $oAuth2Client   = $fb->getOAuth2Client();
        $longLivedToken = $oAuth2Client->getLongLivedAccessToken($token);
        $token          = $longLivedToken->getValue();
        // Reemplazamos el token.
        $this->state->set(
        $module_name . '.page_access_token',
        $token
        );
      }

    }
    catch (ResponseException $e) {
      $this->messenger->addError(
        'Se produjo un error al intentar obtener información del usuario'
      );
    }
    catch (SDKException $e) {
      $this->messenger->addError(
        'Se produjo un error al intentar obtener información del usuario'
      );
    }
    if (isset($user_name)) {
      $this->messenger->addStatus(
          $this->t(
            'Currently, the system has access to @user_name facebook account.',
            ['@user_name' => $user_name]
          )
        );
    }
    if (isset($longLivedToken)) {
      $this->messenger->addStatus(
        $this->t('Replaced access token with a long-lived one')
      );
    }
    if (isset($page_msg)) {
      $this->messenger->addStatus($page_msg);
    }
    return parent::submitForm($form, $form_state);
  }

  /**
   * Form validate.
   *
   * { @inheritDoc }
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Return csv image style.
   *
   * @return string
   *   Return a string with csv imagestyles or empty.
   */
  protected function getImageStyles() {
    $list = \Drupal::entityQuery('image_style')->execute();
    $str = '';
    foreach ($list as $k => $v) {
      if (empty($str)) {
        $str .= "'" . $k . "'";
      }
      else {
        $str .= ", '" . $k . "'";
      }
    }

    return $str;
  }

}
