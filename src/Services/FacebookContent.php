<?php

namespace Drupal\simple_facebook_post\Services;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * FacebookContent class.
 *
 * This class has the function of retrieving or persisting information about the
 * publication of an entity on a facebook page, this helps maintain a
 * relationship between the content created in drupal and the posts made on the
 * facebook page.
 */
class FacebookContent {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * Set method.
   *
   * Save information the database about entity posted.
   *
   * @param int $entity_id
   *   The entity id.
   * @param string $post_content
   *   The text content of post.
   * @param string $post_id
   *   The post id returned by Facebook Graph api as string.
   */
  public function set($entity_id, $post_content, $post_id) {
    $now = new DrupalDateTime('now');

    $fields = [
      'entity_id'     => (int) $entity_id,
      'post_content' => (string) $post_content,
      'post_id'      => (string) $post_id,
      'created'       => $now->getTimestamp(),
    ];

    $insert = $this->connection->insert('sfp_post');
    $insert->fields(
      [
        'entity_id',
        'post_content',
        'post_id',
        'created',
      ],
      $fields
    );

    return $insert->execute();
  }

  /**
   * Get method.
   *
   * Return post information if exist.
   *
   * @param int $entity_id
   *   The entity id.
   */
  public function get(int $entity_id) {
    $query = $this->connection->select('sfp_post', 'fb')
      ->fields(
        'fb',
        [
          'entity_id',
          'post_content',
          'post_id',
          'created',
        ]
      )
      ->condition('entity_id', $entity_id)
      ->execute();

    return $query->fetchObject();
  }

  /**
   * Is Posted method.
   *
   * @param int $entity_id
   *   The entity id to post.
   *
   * @return bool
   *   Return true or false if this entity is alredy posted.
   */
  public function isPosted(int $entity_id) {
    $query = $this->connection->select('sfp_post', 'tw')
      ->fields(
        'tw',
        ['post_id']
      )
      ->condition('entity_id', $entity_id)
      ->execute();
    $result = $query->fetch()->post_id;
    return isset($result) ? TRUE : FALSE;
  }

}
