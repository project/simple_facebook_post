<?php

namespace Drupal\simple_facebook_post\Utils;

use Facebook\Facebook;
use Facebook\Exception\ResponseException;
use Facebook\Exception\SDKException;

/**
 * FacebookWrapper class.
 *
 * This class interact with the sdk.
 */
class FacebookWrapper {

  /**
   * Post in Facebook page method.
   *
   * @return string|false
   *   Return a string with post id or false if error occurred.
   */
  public static function postear($post) {

    $app_id     = \Drupal::state()->get(
      'simple_facebook_post.app_id', ''
    );
    $app_secret = \Drupal::state()->get(
      'simple_facebook_post.app_secret', ''
    );
    $token      = \Drupal::state()->get(
      'simple_facebook_post.page_access_token', ''
    );
    $page_id    = \Drupal::state()->get(
      'simple_facebook_post.page_id', ''
    );

    try {
      $fb           = new Facebook([
        'app_id'                => $app_id,
        'app_secret'            => $app_secret,
        'default_graph_version' => $api_version ?? 'v13.0',
        'default_access_token'  => $token,
      ]);
      $page_token   = '';
      $managedPages = $fb->get('me/accounts?type=page');
      $pages        = $managedPages->getDecodedBody();

      if (isset($pages['data'])) {
        foreach ($pages['data'] as $p) {
          if ($p['id'] == $page_id) {
            $page_token = $p['access_token'];
            break;
          }
        }
      }

      $response = $fb->post(
        '/' . $page_id . '/feed',
        $post,
        $page_token
      );

    }
    catch (ResponseException $e) {
      $err = 'Graph returned an error: ' . $e->getMessage();
      \Drupal::logger('simple_facebook_post')->warning($err);
      return FALSE;
    }
    catch (SDKException $e) {
      $err = 'Facebook SDK returned an error: ' . $e->getMessage();
      \Drupal::logger('simple_facebook_post')->warning($err);
      return FALSE;
    }

    $body = $response->getDecodedBody();
    $post_id = $body['id'] ?? FALSE;
    return $post_id;
  }

  /**
   * Get status of api connection.
   *
   * @return array
   *   Return a array with ['error', 'message']
   */
  public static function getFacebookConnectionStatus() {
    $status = [];
    if (!class_exists('\Facebook\Facebook')) {
      $status['error'] = TRUE;
      $status['message'] = t(
          '<div>
          <p>You need to install the facebook sdk, try installing it using 
          composer:</p>
          <p>composer require "joelbutcher/facebook-graph-sdk:^6.0"</p>
          <p>joelbutcher/facebook-graph-sdk v6.0.0
           requires php-http/client-implementation ^1.0. 
          You can use Guzzle 6 HTTP Adapter, install with composer:</p>
          <p>composer require "php-http/guzzle6-adapter:^2.0"</p>
          </div>'
      );
      return $status;
    }

    $app_id      = \Drupal::state()->get(
      'simple_facebook_post.app_id', ''
    );
    $app_secret  = \Drupal::state()->get(
      'simple_facebook_post.app_secret', ''
    );
    $token       = \Drupal::state()->get(
      'simple_facebook_post.page_access_token', ''
    );
    $page_id     = \Drupal::state()->get(
      'simple_facebook_post.page_id', ''
    );
    $api_version = \Drupal::config('simple_facebook_post.settings')
      ->get('api_version');

    if (empty($app_id) || empty($app_secret) || empty($token)) {
      return '';
    }
    $error = FALSE;
    $markup = '';
    try {

      $fb = new Facebook([
        'app_id'                => $app_id,
        'app_secret'            => $app_secret,
        'default_graph_version' => $api_version ?? 'v13.0',
        'default_access_token'  => $token,
      ]);

      $response_user = $fb->get('/me?fields=name');
      $user_name     = $response_user->getGraphUser()->getName();
      $managedPages  = $fb->get('me/accounts?type=page');
      $pages         = $managedPages->getDecodedBody();
      ;

    }

    catch (ResponseException $e) {
      $error = TRUE;
      $response_err = 'Graph returned an error: ' . $e->getMessage();
      \Drupal::logger('simple_facebook_post')->warning($response_err);

      $markup .= '<p>Se produjo un error al intentar obtener información del 
      usuario, 
      por favor compruebe los datos ingresados sean correctos e 
      intente iniciar sesion con Facebook.</p>
      <pre>@response_exception</pre>';

      $replace['@response_exception'] = $response_err;
    }
    catch (SDKException $e) {
      $error = TRUE;
      $sdk_err = 'Facebook SDK returned an error: ' . $e->getMessage();
      \Drupal::logger('simple_facebook_post')->warning($sdk_err);

      $markup .= '<p>Se produjo un error al intentar obtener información del 
      usuario, 
      por favor compruebe los datos ingresados sean correctos e 
      intente iniciar sesion con Facebook.</p>
      <pre>@response_exception</pre>';

      $replace['@sdk_exception'] = $sdk_err;
    }

    if (!empty($user_name)) {
      $replace['@user_name'] = $user_name;
      $markup .= '<p>Currently, the system has access to @user_name facebook
       account.</p>';
    }

    if (isset($pages['data'])) {
      $page_name = '';

      foreach ($pages['data'] as $p) {
        if ($p['id'] == $page_id) {
          $page_found = 'The @pagename page was entered and the user is
           administrator.';
          $page_name  = $p['name'];

          break;
        }
      }

      if (isset($page_found)) {
        $markup .= $page_found;
        $replace['@pagename'] = $page_name;

      }
      else {
        $error = TRUE;
        $markup .= 'The entered page does not exist or is not managed by the 
        user.';
      }
    }

    $status['error']   = $error;
    $status['message'] = t($markup, $replace);
    return $status;
  }

}
