<?php

namespace Drupal\simple_facebook_post\Utils;

use Drupal\image\Entity\ImageStyle;
use Drupal\node\NodeInterface;

/**
 * PostWrapper class.
 *
 * This static class contains methods that generate the content of a post from
 * an entity.
 */
class PostWrapper {

  /**
   * Get data for create a new post from a entity.
   *
   * @param \Drupal\node\NodeInterface $entity
   *   The entity object.
   */
  public static function getFromEntity(NodeInterface $entity) {
    $config = \Drupal::config('simple_facebook_post.settings');
    $post = [];
    if ($entity->getType() !== $config->get('content')) {
      return NULL;
    }

    $valid_text_types = [
      'string',
      'text',
      'text_with_summary',
      'string_long',
    ];

    if ($config->get('body') === 'title') {
      $post['message'] = $entity->getTitle();
    }
    else {
      $text_field = $entity->{$config->get('body')};
      $field_type = $text_field->getFieldDefinition()->getType();

      if (in_array($field_type, $valid_text_types)) {

        $post['message'] = $text_field->value;
        if (!empty($config->get('body_use_summary'))
          && !empty($text_field->summary)) {
          $post['message'] = $text_field->summary;
        }
      }
    }

    if ($config->get('share_link')) {

      $post['link'] = $entity->toUrl()->setAbsolute(TRUE)->toString();

      $link_title_field = $config->get('link_title');
      $link_title_suff  = $config->get('link_title_suffix');
      $link_img_field   = $config->get('link_image');
      $link_img_style   = $config->get('link_image_style');

      if ($link_title_field) {
        // Cuidado, necesita validar la url de su negocio.
        if ($link_title_field === 'title') {
          $post['name'] = $entity->getTitle();
        }
        else {
          $text_field = $entity->{$config->get('body')};
          $field_type = $text_field->getFieldDefinition()->getType();

          if (in_array($field_type, $valid_text_types)) {
            $post['name'] =
            empty($text_field->summary)
            ? $text_field->value : $text_field->summary;
          }
        }

      }
      if ($link_title_field && !empty($link_title_suff)) {
        $post['name'] .= $link_title_suff;
      }

      if (!empty($link_img_field) && $entity->hasField($link_img_field)) {

        $ref_list = $entity->{$config['image']}->referencedEntities();

        if (isset($ref_list[0])) {
          /** @var \Drupal\file\Entity\File $file */
          $file_uri  = $ref_list[0]->getFileUri();
          $image_uri = $file_uri;

          if (!empty($link_img_style)) {
            $image_uri = ImageStyle::load($link_img_style)
              ->buildUri($file_uri);
          }
          /**
           * @var \Drupal\Core\StreamWrapper\StreamWrapperManager $sw_manager
           * */
          $sw_manager = \Drupal::service('stream_wrapper_manager');
          /**
           * @var \Drupal\Core\StreamWrapper\PublicStream $image_path
           */
          $image_path = $sw_manager->getViaUri($image_uri);

          // $tweetContent->image_path = $stream_wrapper_manager->realpath();
          if ($image_path !== FALSE) {
            // @todo for  v13.0 see usage of 'picture' y/o 'thumbnail'.
            $post['source'] = str_replace(
              'public://',
              $image_path->getDirectoryPath() . '/', $image_uri
            );
          }
        }
      }
    }

    if (isset($post['message'])) {
      $post['message'] = strip_tags($post['message']);
    }

    return $post;
  }

}
